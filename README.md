<div align="center">
<img src="./Screenshot.png">
<hr>

<h1>Frontend Mentor - Article preview component solution</h1>

<p>This is a solution to the <a href="https://www.frontendmentor.io/challenges/article-preview-component-dYBN_pYFT">Article preview component challenge on Frontend Mentor</a>. Frontend Mentor challenges help you improve your coding skills by building realistic projects.
</p>

</div>

<details>
<summary>Table of contents</summary>

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

 </details>

## Overview

this challenge looks simple and it is, but the share button has some accessibility tricks that are hard to implement _for me_, the share button need to interactive with user, and accessible to screen readers

### The challenge

Users should be able to:

- View the optimal layout for the component depending on their device's screen size
- See the social media share links when they click the share icon

### Screenshot

---

<table align="center">
<tr>
<td>
<h2>Mobile Version</2>
<img src ="./Screenshot_mobile.png"/>
</td>
<td>
<h2>Desktop Version</h2>
<img src="./Screenshot.png" />
</td>
</tr>
</table>

---

### Links

- Solution URL: [ solution URL ](https://www.frontendmentor.io/solutions/article-preview-component-solution-using-flex-BaKqapassY)
- Live Site URL: [live site](https://leafy-hotteok-f86b78.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### What I learned

trying to make share `button` accessible, was really challenging _I need to learn more about accessibility_, aside from that I learned how to Manipulate DOM using `data-attribute`, and control appearance of elements using `CSS`,
Some snippets, see below:

```html
<h1 data-example="false">Some HTML code I'm proud of</h1>
```

```css
h1 {
  color: papayawhip;
}

h1[data-example="true"] {
  color: gray;
}
```

```js
const example1 = document.querySelector("[data-example]");
const isTrue = example.getAttribute("data-example");
if (isTrue === "false") {
  example.setAttribute("data-example", "true");
} else {
  example.setAttribute("data-example", "false");
}
```

### Continued development

well they is no need to continued development to this project,however I will come back to the share `button` if Im abele to implement accessible solution to it.

### Useful resources

- [clip-path generator](https://bennettfeely.com/clippy/) - This helped me generate `clip-path: polygon(1% 1%, 50% 99%, 100% 0);` for the share lunks .

## Author

- Website - [imad bg](https://imadbg01.github.io/)
- Frontend Mentor - [@imadbg01](https://www.frontendmentor.io/profile/imadbg01)
- Twitter - [@ImadBg4](https://twitter.com/ImadBg4)

## Acknowledgments

big Thanks to all people ho give feedback on my [solution](https://www.frontendmentor.io/solutions/article-preview-component-solution-using-flex-BaKqapassY)
